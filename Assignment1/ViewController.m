//
//  ViewController.m
//  Assignment1
//
//  Created by Jason Clinger on 1/12/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIScrollView* scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollView.backgroundColor = [UIColor blueColor];
    [self.view addSubview:scrollView];
    
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height * 3)];
    
//    UIView* redView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
//    
//    redView.backgroundColor = [UIColor redColor];
//    [scrollView addSubview:redView];
//    
//    UIView* whiteView = [[UIView alloc]initWithFrame:CGRectMake(0,0, scrollView.frame.size.width, scrollView.frame.size.height)];
//    whiteView.backgroundColor = [UIColor whiteColor];
//    [scrollView addSubview:whiteView];
    
    UIView* view = [[UIView alloc]initWithFrame:CGRectMake(scrollView.frame.size.width * 1,0, scrollView.frame.size.width, scrollView.frame.size.height)];
    
    view.backgroundColor = [UIColor yellowColor];
    [scrollView addSubview:view];
    
    UIView* redView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
    
    redView.backgroundColor = [UIColor redColor];
    [scrollView addSubview:redView];
    
    UIView* whiteView = [[UIView alloc]initWithFrame:CGRectMake(scrollView.frame.size.width * 2,0, scrollView.frame.size.width, scrollView.frame.size.height)];
    whiteView.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:whiteView];
    
    UIView* orangeView = [[UIView alloc]initWithFrame:CGRectMake(0, scrollView.frame.size.height * 1, scrollView.frame.size.width, scrollView.frame.size.height)];
    orangeView.backgroundColor = [UIColor orangeColor];
    [scrollView addSubview:orangeView];
    
    UIView* grayView = [[UIView alloc]initWithFrame:CGRectMake(0, scrollView.frame.size.height * 2, scrollView.frame.size.width, scrollView.frame.size.height)];
    grayView.backgroundColor = [UIColor grayColor];
    [scrollView addSubview:grayView];
    
    UIView* greenView = [[UIView alloc]initWithFrame:CGRectMake(scrollView.frame.size.width * 1, scrollView.frame.size.height * 1, scrollView.frame.size.width, scrollView.frame.size.height)];
    greenView.backgroundColor = [UIColor greenColor];
    [scrollView addSubview:greenView];
    
    UIView* purpleView = [[UIView alloc]initWithFrame:CGRectMake(scrollView.frame.size.width * 1, scrollView.frame.size.height * 2, scrollView.frame.size.width, scrollView.frame.size.height)];
    purpleView.backgroundColor = [UIColor purpleColor];
    [scrollView addSubview:purpleView];
    
    UIView* brownView = [[UIView alloc]initWithFrame:CGRectMake(scrollView.frame.size.width * 2, scrollView.frame.size.height * 1, scrollView.frame.size.width, scrollView.frame.size.height)];
    brownView.backgroundColor = [UIColor brownColor];
    [scrollView addSubview:brownView];
    
    UIView* lightGrayView = [[UIView alloc]initWithFrame:CGRectMake(scrollView.frame.size.width * 2, scrollView.frame.size.height * 2, scrollView.frame.size.width, scrollView.frame.size.height)];
    lightGrayView.backgroundColor = [UIColor lightGrayColor];
    [scrollView addSubview:lightGrayView];
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
